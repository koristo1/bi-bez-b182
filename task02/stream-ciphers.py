#!/usr/bin/python

import sys
import random

from Crypto.Cipher import ARC4
from Crypto import Random

KEYSTREAM_SIZE = 64
ENCRYPT_FLAG = '-e'
DECRYPT_FLAG = '-d'


class InvalidArgumentCount(Exception):
    pass


class InvalidFlag(Exception):
    pass


def read_arguments():
    arguments = sys.argv
    if len(arguments) != 2:
        raise InvalidArgumentCount
    flag = arguments[1]
    if flag != ENCRYPT_FLAG and flag != DECRYPT_FLAG:
        raise InvalidFlag

    return flag


def read_hex(counter):
    line = input("Enter cipher text #" + str(counter) + ": ")

    if len(line) % 2 == 1:  # odd count of bytes
        line = "0" + line

    return bytearray.fromhex(line)


def cut_tail(data, count):
    return data[:count]


def generate_keystream():

    key = b"\x00" + random.getrandbits(KEYSTREAM_SIZE) + b"\x00"

    return key


def pad_plain_text(plain_text):
    return 0 * KEYSTREAM_SIZE - len(plain_text) + plain_text


def encrypt():
    plain_text_1 = b"This book was written using 100% recycled words."
    plain_text_2 = b"It's the difference between using a feather and using a chicken."
    rand = Random.new()
    key = rand.read(8)

    cipher = ARC4.new(key)

    cipher_text_1 = cipher.encrypt(plain_text_1)
    cipher = ARC4.new(key)
    cipher_text_2 = cipher.encrypt(plain_text_2)

    print(plain_text_2.decode('ascii'), cipher_text_1.hex(), cipher_text_2.hex(), sep='\n')


def xor(a, b):  # assumes identical length of a and b
    # bytes()
    return [i ^ j for i, j in zip(a, b)]


def decrypt():
    plain_text = input("Enter plain text:").encode()
    cipher_text_1 = read_hex(0)
    cipher_text_2 = read_hex(1)

    minimum = min([len(plain_text), len(cipher_text_1), len(cipher_text_2)])  # cut to shortest

    plain_text = cut_tail(plain_text, minimum)
    cipher_text_1 = cut_tail(cipher_text_1, minimum)
    cipher_text_2 = cut_tail(cipher_text_2, minimum)

    xored_ciphers = xor(cipher_text_1, cipher_text_2)

    result = bytes(xor(plain_text, xored_ciphers))
    print(result.decode('ascii'))


def main():
    flag = read_arguments()
    encrypt() if flag == ENCRYPT_FLAG else decrypt()

main()

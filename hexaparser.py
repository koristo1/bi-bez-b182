import binascii


# fc57c9cfdac7202694e8c2712d1f0e8e21b8c6f6d612d6c96847e42e6257b3f9ea98466cfc8761ed5e0308311deb15
# a83fa0bcfaae5306f5c8b1144e6d6bfa01cca38ea23cf68707258b4a1b77c09185ed2a08dcf5048c3a237c5974983b

# Test string
# 5465737420737472696e67
def read_hex(number):
    print("Enter string #" + str(number) + ":", end='')
    line = input()

    if len(line) % 2:
        line = "0" + line
    return line


def write_ascii(result):
    for x in result:
        print(str(chr(x)), end='')


def write_hex(line):
    result = bytearray(line, 'utf-8')
    print(binascii.hexlify(result))


def main():
    parsed = False
    while not parsed:

        try:
            print("Enter string #" + "1" + ":", end='')
            line = input()
            byte_line = line
            if len(byte_line) % 2:
                byte_line = "0" + byte_line
            byte_line1 = bytearray.fromhex(byte_line)
        except ValueError:
            write_hex(line)
            return
        try:
            byte_line2 = bytearray.fromhex(read_hex(2))
            parsed = True
        except ValueError:
            print("Invalid input, try again.")

    result = [(i ^ j) for i, j in zip(byte_line1, byte_line2)]

    write_ascii(result)
    print()


main()

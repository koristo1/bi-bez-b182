#include <stdio.h>
#include <string.h>
#include <openssl/evp.h>
#include <cstdlib>
#include <string>
#include <iostream>
#include <iomanip>

using namespace std;

//Hash textu "8Lw" je: aabb07b4e441faf638e732de34525de49b42ffcd

const auto DEPTH_MAX = 2;
static const string alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOQRSTUVWXYZ0123456789";


void hashThat(const char *text) {
    int i, res;
    auto hashFunction = "sha1";  // zvolena hashovaci funkce ("sha1", "md5" ...)

    EVP_MD_CTX *ctx;  // struktura kontextu
    const EVP_MD *type; // typ pouzite hashovaci funkce
    unsigned char hash[EVP_MAX_MD_SIZE]; // char pole pro hash - 64 bytu (max pro sha 512)
    int length;  // vysledna delka hashe

    /* Inicializace OpenSSL hash funkci */
    OpenSSL_add_all_digests();
    /* Zjisteni, jaka hashovaci funkce ma byt pouzita */
    type = EVP_get_digestbyname(hashFunction);

    /* Pokud predchozi prirazeni vratilo -1, tak nebyla zadana spravne hashovaci funkce */
    if (!type) {
        cout << "Hash " << hashFunction << "neexistuje." << endl;
        exit(1);
    }

    ctx = EVP_MD_CTX_create(); // create context for hashing
    if (ctx == nullptr)
        exit(2);

    /* Hash the text */
    res = EVP_DigestInit_ex(ctx, type, nullptr); // context setup for our hash type
    if (res != 1)
        exit(3);
    res = EVP_DigestUpdate(ctx, text, strlen(text)); // feed the message in
    if (res != 1)
        exit(4);
    res = EVP_DigestFinal_ex(ctx, hash, (unsigned int *) &length); // get the hash
    if (res != 1)
        exit(5);

    EVP_MD_CTX_destroy(ctx); // destroy the context

    /* Vypsani vysledneho hashe */
    cout << "Hash textu \"" << setw(DEPTH_MAX) << text << "\" je: ";
    for (i = 0; i < length; i++)
        printf("%02x", hash[i]);
    cout << endl;
}

void rec(const string &text, int depth) {
    if (depth == DEPTH_MAX) {
        return;
    }

    for (char c : alphabet) {
        hashThat((text + c).c_str());
        rec(text + c, depth + 1);
    }
}

int main(int argc, char *argv[]) {
    rec("", 0);

    exit(0);
}
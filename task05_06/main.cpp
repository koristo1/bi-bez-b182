#include <iostream>
#include <fstream>
#include <openssl/ssl.h>
#include <unistd.h>
#include <netdb.h>

using namespace std;

static const string HOST = "fit.cvut.cz";
static const string PORT = "443";
static const string GET_COMMAND = "GET /student/odkazy HTTP/1.0\n\n";
static const string INDEX_FILE_NAME = "index.html";
static const string CERTIFICATES_BUNDLE_NAME = "/etc/ssl/ca-bundle.pem";
static const string CERTIFICATE_FILE_NAME = "certificates.PEM";
static const int BUFFER_SIZE = 1024;

int createSocket() {
    int socketID;
    struct addrinfo *addressInfo;
    if (getaddrinfo(HOST.c_str(), PORT.c_str(), nullptr, &addressInfo) != 0)
        return -1;
    if ((socketID = socket(addressInfo->ai_family, SOCK_STREAM, IPPROTO_TCP)) < 0) {
        freeaddrinfo(addressInfo);
        return -1;
    }
    if (connect(socketID, addressInfo->ai_addr, addressInfo->ai_addrlen) == -1) {
        close(socketID);
        freeaddrinfo(addressInfo);
        return -1;
    }
    return socketID;
}

int cleanUp(SSL *ssl, SSL_CTX *context, int socket) {
    if (ssl) {
        SSL_shutdown(ssl);
        SSL_free(ssl);
    }
    if (context)
        SSL_CTX_free(context);
    close(socket);
    return 1;
}

void emptyBuffer(char *buffer) {
    for (int i = 0; i < BUFFER_SIZE; ++i)
        buffer[i] = '\0';
}

int main() {
    
    SSL_CTX *context = nullptr;
    SSL *ssl = nullptr;
    X509 *certificate = nullptr;
    FILE *file = nullptr;
    ofstream outputFile;
    char buffer[BUFFER_SIZE + 1];

    emptyBuffer(buffer);

    int socket = createSocket();
    if (socket == -1) {
        cout << "Cannot connect." << endl;
        return 1;
    }
    if (!SSL_library_init()) {
        cout << "Error initialising SSL  library." << endl;
        return cleanUp(ssl, context, socket);
    }

    context = SSL_CTX_new(SSLv23_client_method());

    if (!SSL_CTX_load_verify_locations(context, CERTIFICATES_BUNDLE_NAME.c_str(), nullptr)) {
        cout << "Error loading local certificates." << endl;
        return cleanUp(ssl, context, socket);
    }
    SSL_CTX_set_options(context, SSL_OP_NO_SSLv2 | SSL_OP_NO_SSLv3 | SSL_OP_NO_TLSv1);
    ssl = SSL_new(context);
    SSL_set_fd(ssl, socket);
    SSL_set_cipher_list(ssl, "ALL:!ECDHE-RSA-AES256-GCM-SHA384");
    if (!SSL_connect(ssl)) {
        cout << "SSL connection error." << endl;
        return cleanUp(ssl, context, socket);
    }
    long int code = SSL_get_verify_result(ssl);

    if (code != X509_V_OK) {
        cout << "Error verifying certificate. Returned = " << code << "." << endl;
        return cleanUp(ssl, context, socket);
    }
    if (SSL_write(ssl, GET_COMMAND.c_str(), GET_COMMAND.length()) < (int) GET_COMMAND.length())
        return cleanUp(ssl, context, socket);

    outputFile.open(INDEX_FILE_NAME, ofstream::out);
    while (SSL_read(ssl, buffer, BUFFER_SIZE) > 0) {
        outputFile << buffer << endl;
        emptyBuffer(buffer);
    }

    certificate = SSL_get_peer_certificate(ssl);
    if (!certificate)
        cout << "Error accessing certificate." << endl;

    file = fopen(CERTIFICATE_FILE_NAME.c_str(), "w");
    if (file == nullptr) {
        cout << "Error opening certificate file" << endl;
        return cleanUp(ssl, context, socket);
    }

    if (!PEM_write_X509(file, certificate)) {
        cout << "Error writing certificate." << endl;
        return cleanUp(ssl, context, socket);
    }

    cout << "Used certificate" << endl;
    cout << SSL_CIPHER_get_name(SSL_get_current_cipher(ssl)) << endl;
    const char *cipherName;

    for (size_t i = 0; (cipherName = SSL_get_cipher_list(ssl, i)); ++i)
        cout << "(" << i << ") " << cipherName << endl;
    cleanUp(ssl, context, socket);
    return 0;
}

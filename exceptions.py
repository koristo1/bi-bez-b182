#!/usr/bin/python

class InvalidHeaderContent(Exception):
    pass


class InvalidArgumentCount(Exception):
    pass


class InvalidOperationFlag(Exception):
    pass


class InvalidCipherModeFlag(Exception):
    pass


class FileSizeMismatch(Exception):
    pass

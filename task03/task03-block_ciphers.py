# Koristka Tomas: koristo1@fit.cvut.cz
# !/usr/bin/env python3

import binascii
import os
import sys

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes


class InvalidHeaderContent(Exception):
    pass


class InvalidArgumentCount(Exception):
    pass


class InvalidOperationFlag(Exception):
    pass


class InvalidCipherModeFlag(Exception):
    pass


class FileSizeMismatch(Exception):
    pass


ENCRYPT_FLAG = '-e'
DECRYPT_FLAG = '-d'
ECB_FLAG = 'ecb'
CBC_FLAG = 'cbc'

CHUNK_SIZE = 1000


def crypto(cryptor, input_file, output_file):
    with open(input_file, mode="br") as input_image:
        input_image.seek(10)  # skip header to start
        start = int.from_bytes(input_image.read(4), byteorder="little")  # read start
        input_image.seek(0)  # reset
        with open(output_file, mode="bw") as output_image:
            output_image.write(input_image.read(start))  # write header
            while True:
                plain_text = input_image.read(CHUNK_SIZE)
                if not plain_text:  # err
                    break
                cipher_data = cryptor.update(plain_text)  # update
                output_image.write(cipher_data)  # save
            cipher_data = cryptor.finalize()
            output_image.write(cipher_data)


def read_arguments():
    arguments = sys.argv
    if len(arguments) != 4:
        raise InvalidArgumentCount

    operation_flag = arguments[1]

    if operation_flag != ENCRYPT_FLAG and operation_flag != DECRYPT_FLAG:
        raise InvalidOperationFlag

    mode_flag = arguments[2]

    if mode_flag != ECB_FLAG and mode_flag != CBC_FLAG:
        raise InvalidCipherModeFlag

    image_path = arguments[3]

    return operation_flag, mode_flag, image_path


def parse_header(image_path):
    with open(image_path, 'rb') as FILE:
        buffer = FILE.read(14)

    magic_number = buffer[0:2]
    if magic_number != b'BM':
        raise InvalidHeaderContent

    byte_size = buffer[2:6]
    size = int.from_bytes(byte_size, byteorder='little')

    if size != os.stat(image_path).st_size:
        raise FileSizeMismatch

    begin = buffer[10:14]

    return buffer, size, int.from_bytes(begin, byteorder='little')


def save_key_and_iv(key, iv, file_name):
    with open(remove_extension(file_name) + '_key', 'wb') as FILE:
        FILE.write(key)
        FILE.write(iv)


def remove_extension(input_image_path):
    filename = input_image_path.split('.')[0]
    return filename


def construct_output_path(input_image_path, mode, operation):
    if operation == ENCRYPT_FLAG:
        return remove_extension(input_image_path) + '_' + ('ecb' if mode == ECB_FLAG else 'cbc') + '.bmp'
    else:
        return remove_extension(input_image_path) + '_' + 'dec' + '.bmp'


def write_header(output_image_name, header):
    with open(output_image_name, 'wb') as FILE:
        FILE.write(header)


def crypt(key, iv, input_image_path, output_image_path, operation, mode):
    key, iv
    backend = default_backend()
    if operation == ENCRYPT_FLAG:
        print("Mode:", mode)
        print(f"Encryption key: {binascii.hexlify(key)}")
        if mode == CBC_FLAG:
            print(f"Initial vector: {binascii.hexlify(iv)}")
        if mode == ECB_FLAG:
            cipher = Cipher(algorithms.AES(key), modes.ECB(), backend=backend)
        else:
            cipher = Cipher(algorithms.AES(key), modes.CBC(iv), backend=backend)
        encryptor = cipher.encryptor()
        crypto(encryptor, input_image_path, output_image_path)

    else:
        key = input("Enter key : ")
        if mode == CBC_FLAG:
            iv = input("Enter IV: ")
            cipher = Cipher(algorithms.AES(bytes.fromhex(key)), modes.CBC(bytes.fromhex(iv)), backend=backend)
        else:
            cipher = Cipher(algorithms.AES(bytes.fromhex(key)), modes.ECB(), backend=backend)
        decryptor = cipher.decryptor()
        crypto(decryptor, input_image_path, output_image_path)


operation, mode, input_image_path = read_arguments()

header, file_size, begin = parse_header(input_image_path)

output_image_path = construct_output_path(input_image_path, mode, operation)

key = os.urandom(32)
iv = os.urandom(16)

crypt(key, iv, input_image_path, output_image_path, operation, mode)

#include <bits/stdc++.h>
#include <openssl/evp.h>
#include <openssl/pem.h>
#include <openssl/rand.h>

using namespace std;


const int CHUNK_SIZE = 1024;

class FileException {
};

class Exception {
};

struct Result {
    uint8_t *data;
    uint32_t bytes;

    Result(uint8_t *data, uint32_t bytes) : data(data), bytes(bytes) {}
};

class PictureParser {
public:
    ifstream file;
    uint64_t length;
    uint64_t read;

    PictureParser(string &filename) : read(0) {
        file.open(filename, ios::binary);
        if (!file.good()) {
            cerr << "Error: " << strerror(errno);
            throw FileException();
        }
        retrieveLength();
    }

    Result getChunk(int chunk) {
        uint8_t *data = (uint8_t *) malloc(chunk);
        file.read((char *) data, chunk);
        streamsize size = file.gcount();
        read += size;
        return Result(data, size);
    }

    bool hasData() { return read != length; }

    ~PictureParser() { file.close(); }

private:
    void retrieveLength() {
        file.seekg(0, ios::end);
        length = (uint64_t) file.tellg();
        file.seekg(0);
    }
};

int main(int argc, char *argv[]) {
    if (argc != 4) {
        cerr << "Missing arguments" << endl;
        exit(1);
    }
    int flag;
    EVP_PKEY *privateKey;
    FILE *privateKeyFile;
    const EVP_CIPHER *cipher;
    string outputFileName;
    string inputFileName;
    string privateKeyName;
    EVP_CIPHER_CTX *ctx; // context structure
    unsigned char *iv, *key;
    char *cipherName;
    int *length;
    inputFileName = argv[1];
    outputFileName = argv[2];
    privateKeyName = argv[3];

    PictureParser parser(inputFileName);

    OpenSSL_add_all_ciphers();

    cipherName = (char *) parser.getChunk(128 * sizeof(char)).data;
    length = (int *) parser.getChunk(sizeof(int)).data;
    key = parser.getChunk(*length).data;
    iv = parser.getChunk(EVP_MAX_IV_LENGTH).data;

    ctx = EVP_CIPHER_CTX_new(); //
    EVP_CIPHER_CTX_init(ctx);
    cipher = EVP_get_cipherbyname(cipherName);


    privateKeyFile = fopen(privateKeyName.c_str(), "r");
    privateKey = PEM_read_PrivateKey(privateKeyFile, nullptr, nullptr, nullptr);

    int tmpLength = 0;

    ofstream outputFile(outputFileName, ios::binary);
    flag = EVP_OpenInit(ctx, cipher, key, *length, iv, privateKey);
    if (flag != 1)
        exit(2);


    while (parser.hasData()) {
        Result result = parser.getChunk(CHUNK_SIZE + EVP_MAX_BLOCK_LENGTH);
        auto *plainText = (unsigned char *) malloc(2 * CHUNK_SIZE + EVP_MAX_BLOCK_LENGTH);
        flag = EVP_OpenUpdate(ctx, plainText, &tmpLength, result.data, result.bytes);
        if (flag != 1)
            exit(2);

        outputFile.write((char *) plainText, tmpLength);
    }
    auto *plainText = (unsigned char *) malloc(2 * CHUNK_SIZE + EVP_MAX_BLOCK_LENGTH);
    flag = EVP_OpenFinal(ctx, plainText, &tmpLength);
    if (flag != 1)
        exit(2);

    outputFile.write((char *) plainText, tmpLength);
    outputFile.close();
    free(plainText);
}

#include <bits/stdc++.h>
#include <openssl/evp.h>
#include <openssl/pem.h>
#include <openssl/rand.h>
#include <exception>

using namespace std;


const int CHUNK_SIZE = 1024;

class FileException : public exception {
};

class Exception : public exception {
};

struct Result {
    uint8_t *data;
    uint32_t bytes;

    Result(uint8_t *data, uint32_t bytes) : data(data), bytes(bytes) {}
};

class PictureParser {
public:

    PictureParser(string &filename) : read(0) {
        file.open(filename, ios::binary);
        if (!file.good()) {
            cerr << "Error: " << strerror(errno);
            throw FileException();
        }
        retrieveLength();
    }

    Result getChunk(int chunk) {
        uint8_t *data = (uint8_t *) malloc(chunk);
        file.read((char *) data, chunk);
        streamsize size = file.gcount();
        read += size;
        return Result(data, size);
    }

    bool hasData() { return read != length; }

    ~PictureParser() { file.close(); }

private:
    ifstream file;
    uint64_t length;
    uint64_t read;

    void retrieveLength() {
        file.seekg(0, ios::end);
        length = (uint64_t) file.tellg();
        file.seekg(0);
    }
};

int main(int argc, char *argv[]) {
    if (argc != 5) {
        cerr << "Missing arguments" << endl;
        exit(1);

    }

    int flag;
    EVP_PKEY *publicKey;
    FILE *privateKeyFile;
    const EVP_CIPHER *cipher;
    string inputFileName;
    string outputFileName;
    string cipherName;
    string privateKeyName;
    EVP_CIPHER_CTX *context; // context structure
    unsigned char iv[EVP_MAX_IV_LENGTH];
    inputFileName = argv[1];
    outputFileName = argv[2];
    cipherName = argv[3];
    privateKeyName = argv[4];

    // parsing file
    PictureParser parser(inputFileName);

    OpenSSL_add_all_ciphers();

    context = EVP_CIPHER_CTX_new();
    EVP_CIPHER_CTX_init(context);
    cipher = EVP_get_cipherbyname(cipherName.c_str());


    privateKeyFile = fopen(privateKeyName.c_str(), "r");
    publicKey = PEM_read_PUBKEY(privateKeyFile, nullptr, nullptr, nullptr);

    if (RAND_load_file("/dev/random", 32) != 32) {
        puts("Cannot seed the random generator!");
        exit(1);
    }


    auto *encryptionKey = (unsigned char *) malloc(
            EVP_PKEY_size(publicKey)); // allocate space for encrypted symmet. key
    int encryptionKeyLength; // enc. sym. key length


    int outputLength = 0;

    EVP_SealInit(context, cipher, &encryptionKey, &encryptionKeyLength, iv, &publicKey, 1);
    ofstream outputFile(outputFileName, ios::binary);

    outputFile.write(cipherName.c_str(), 128);
    outputFile.write((const char *) &encryptionKeyLength, sizeof(int));
    outputFile.write((const char *) encryptionKey, encryptionKeyLength);
    outputFile.write((const char *) iv, EVP_MAX_IV_LENGTH);

    //Read data and encrypt them
    while (parser.hasData()) {
        Result result = parser.getChunk(CHUNK_SIZE + EVP_MAX_BLOCK_LENGTH);
        auto plainText = (unsigned char *) malloc(2 * CHUNK_SIZE + EVP_MAX_BLOCK_LENGTH);
        flag = EVP_SealUpdate(context, plainText, &outputLength, result.data, result.bytes);
        if (flag != 1)
            throw Exception();
        outputFile.write((char *) plainText, outputLength);
        free(plainText);
        free(result.data);
    }

    // encrypting last chunk of data
    auto *plain_text = (unsigned char *) malloc(2 * CHUNK_SIZE + EVP_MAX_BLOCK_LENGTH);
    flag = EVP_SealFinal(context, plain_text, &outputLength);
    if (flag != 1)
        throw Exception();
    outputFile.write((char *) plain_text, outputLength);
    outputFile.close();
    free(plain_text);

}
